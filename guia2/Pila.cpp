#include <iostream>
#include "Pila.h"
#include "Contenedor.h"


using namespace std;


Pila::Pila (int Tamano){
	this->Tamano = Tamano;
	this->pila = new int [this->Tamano];
}

int Pila::get_Tamano(){
	return this->Tamano;
}

void Pila::set_Tamano(int Tamano){
	this->Tamano = Tamano;
}

void Pila::pila_vacia(){
	
	if (Tope == 0){
		Band = true;
	}
	else{
		Band = false;
	} 
}

void Pila::pila_llena(){
	
	if (Tope == Tamano){
		Band = true;
	}
	else{
		Band = false;
	}
}

void Pila::Push(){
	int numeros_ingresados;
	
	pila_llena();
	
	if (Band == true){
		cout << "La pila se encutra llena, \nelimine algun elemento para ingresar otro dato." << endl; 
	}
	
	else{
		for (int i=1; i <= Tamano; i++){
			cout << "ingrese un numero: " << endl;
			cin >> numeros_ingresados;
			Tope++;
			pila[Tope] = numeros_ingresados;
			
		}	
	}
}

void Pila::Pop(){
	int Dato;
	
	pila_vacia();
	
	if (Band == true){
		cout << "La pila esta vacia, ingrese algun dato para poder elminar." << endl;
	}
	else{
		Dato = pila[Tope];
		Tope = Tope - 1;
		cout << "El numero eliminado es: " << Dato << endl;
	}
}

void Pila::Recorrer_pila(){
	
	cout << "Los elementos de la pila son: " << endl; 
	for(int i=1; i<=Tope; i++){
		cout << pila[i] << endl; 
	}

}
