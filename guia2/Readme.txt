-Empezando el programa: este trata sobre un puerto seco, al inciar nos pedira un largo y un ancho del puerto, el cual solo sera posible ingresarle numero naturales. Luego nos dara 4 opciones, la primera es agregar contenedores al puerto, la segunda nos dejara eliminar alguna, la tercera nos imprimira como esta el puerto, y la cuarta nos cerrara el programa.

Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia2algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

Sistema operativo:
Debian GNU/Linux 10 (buster)

Autor:
Bryan Ahumada Ibarra


