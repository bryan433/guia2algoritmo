#include <iostream>

using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
	
	private:
		string nombre;
		string empresa;
		
	public:
		Contenedor();
		void anadir_datos();
		string get_nombre();
		string get_empresa();
		
};
#endif
