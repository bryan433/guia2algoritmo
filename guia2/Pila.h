#include <iostream>
#include "Contenedor.h"	

using namespace std;

#ifndef PILA_H
#define PILA_H


class Pila{
	
	private:
		int Tamano;
		int *pila = NULL;
		int Tope = 0;
		bool Band;
	
	public:
		Pila (int Tamano);
		int get_Tamano();
		void set_Tamano(int Tamano);
		void pila_vacia();
		void pila_llena();
		void Push();
		void Pop();
		void Recorrer_pila();
	
	

};
#endif


