#include <iostream>
#include "Pila.h"
#include "Contenedor.h"


using namespace std;

int main (void){
	int N;
	bool inicio = true;
	int opcion;
	
	cout <<"ingrese el tamaño de la pila: " << endl;
	cin >> N;
	system ("clear");
	Pila d = Pila(N);
	while (inicio){
		
		cout <<"Agregar elementos/Push [1]" << endl;
		cout <<"Remover elementos/Pop [2]" << endl;
		cout <<"Ver datos de la pila [3]" << endl; 
		cout <<"Salir [4]" << endl;
		cin >> opcion;
		
		switch(opcion){
			
			case 1:{
				system("clear");
				d.Push();
				break;
			}
			case 2:{
				system("clear");
				d.Pop();
				break;
			}
			case 3:{
				system("clear");
				d.Recorrer_pila();
				break;
			}
			case 4:{
				system("clear");
				exit(0);
				break;
			}
		}
	}
	
}
